'use strict';
const webpack = require('webpack');

module.exports = {
    node: {
        global: false,
        path: false,
        crypto: true,
        fs: 'empty'
    }
}
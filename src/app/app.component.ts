import { Component, OnInit } from '@angular/core';
import SwaggerUI from 'swagger-ui'
import SwaggerUIBundle from 'swagger-ui'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  
  title = 'swagger';
  swagger = 'https://petstore.swagger.io/v2/swagger.json';


  ngOnInit() {
    this.createNEwSwagger();
    
  }

  mudarJson() {
    this.swagger = 'https://app.pricefy.com.br:443/PricefyPartner/swagger/docs/v1'
    this.createNEwSwagger();
  }

  createNEwSwagger() {
    const ui = SwaggerUIBundle({
      url: this.swagger,
      dom_id: '#swagger-ui',
      presets: [
        SwaggerUIBundle.presets.apis,
        SwaggerUIBundle.SwaggerUIStandalonePreset
      ]
    })
  }
}
